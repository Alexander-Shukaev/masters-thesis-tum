% Modules {{{
% ==============================================================================
\usemodule[algorithmic]
% \usemodule[flowchart]
\usemodule[pgfplots]
\usemodule[tikz]
% \usemodule[tikz-uml]
% \usemodule[tikzscale]
% ------------------------------------------------------------------------------
% \usetikzlibrary[external]
\usetikzlibrary[patterns]
\usetikzlibrary[positioning]
\usetikzlibrary[shapes.misc]
\usetikzlibrary[shapes.multipart]
\usetikzlibrary[shapes]
% ==============================================================================
% }}} Modules

% Document {{{
% ==============================================================================
\setupdocument[
  author={Alexander K. Shukaev},
  % date={\currentdate},
  % date={\date},
    date={\date[d={11}, m={6}, y={2015}]},
   title={A Fully Parallel Process-to-Process Intercommunication Technique for
     preCICE},
]
% ==============================================================================
% }}} Document

% Interaction {{{
% ==============================================================================
\setupinteraction[
  author={\documentvariable{author}},
    date={\documentvariable{date}},
   title={\documentvariable{title}},
]
% ------------------------------------------------------------------------------
\setupinteraction[
          color={default},
  contrastcolor={default},
          state={start},
          style={normal},
]
% ------------------------------------------------------------------------------
% \showframe
% ------------------------------------------------------------------------------
\enabledirectives[destinations.log]
% ------------------------------------------------------------------------------
\enabledirectives[references.border={red}]
% ==============================================================================
% }}} Interaction

% Paper Size {{{
% ==============================================================================
\setuppapersize[A4][A4]
% ==============================================================================
% }}} Paper Size

% Layout {{{
% ==============================================================================
\setuplayout[
   topspace={1in},
  backspace={1in},
]
% ------------------------------------------------------------------------------
\setuplayout[
  footer={\interlinespaceparameter{line}},
  header={\interlinespaceparameter{line}},
  margin={\zeropoint},
]
% ------------------------------------------------------------------------------
\setuplayout[
  footerdistance={5mm},
  headerdistance={5mm},
  margindistance={\zeropoint},
]
% ------------------------------------------------------------------------------
\setuplayout[
% height={fit},
  height={middle},
   width={
     \dimexpr                                                                  %
     \paperwidth                                                               %
     -                                                                         %
     2 \backspace                                                              %
     \relax
   },
]
% ==============================================================================
% }}} Layout

\setupfooter[state={start}]
\setupheader[state={none}]

% Fonts {{{
% ==============================================================================
\definefontfamily[mainface][serif][Times New Roman]
\definefontfamily[mainface][sans] [Arial]
\definefontfamily[mainface][mono] [Consolas]
\definefontfamily[mainface][math] [Latin Modern Math]

% \definebodyfontenvironment[default][tt={\small\mono}]
% ------------------------------------------------------------------------------
\setupbodyfont[mainface, 12pt]
% ==============================================================================
% }}} Fonts

% Interline Space {{{
% ==============================================================================
\setupinterlinespace
[line={2.8ex}]
% ==============================================================================
% }}} Interline Space

% Whitespace {{{
% ==============================================================================
\setupwhitespace
[line]
% ==============================================================================
% }}} Whitespace

% Blank {{{
% ==============================================================================
\setupblank
[line]
% ==============================================================================
% }}} Blank

% Indenting {{{
% ==============================================================================
\setupindenting[no]
% ==============================================================================
% }}} Indenting

% Page Numbering {{{
% ==============================================================================
\setuppagenumbering
[location={footer, middle}]
% ==============================================================================
% }}} Page Numbering

% Main Language {{{
% ==============================================================================
\mainlanguage
[en]
% ------------------------------------------------------------------------------
\language
[en]
% ==============================================================================
% }}} Main Language

\setbreakpoints[compound]

% Alignment {{{
% ==============================================================================
\setupalign
[nohz, hyphenated, morehyphenation, verytolerant]
% ==============================================================================
% }}} Alignment

% Label Texts {{{
% ==============================================================================
% \setuplabeltext
% [ru]
% [appendix={Приложение~}]
% ------------------------------------------------------------------------------
% \setuplabeltext
% [ru]
% [listing={Листинг~}]
% ==============================================================================
% }}} Label Texts

% Symbols {{{
% ==============================================================================
\definesymbol
[emdash]
[{---}]
% ==============================================================================
% }}} Symbols

% Headings {{{
% ==============================================================================
\setuphead[%
       align={flushleft, nothyphenated, verytolerant},
  indentnext={no},
]
% ------------------------------------------------------------------------------
\setuphead
[chapter][%
  % When entering a page in normal typesetting mode, ConTeXt is in vertical
  % mode, and will automatically gobble all vertical whitespace that follows
  % immediately in order to align the first line of the text to the top of the
  % page.  That's why normally one does not see an extra vertical whitespace
  % before (above) the title which begins the page.  However, this is not the
  % case when entering the `makeup' environment since ConTeXt is in horizontal
  % mode then, and, as a result, the extra vertical whitespace before (above)
  % the title is not automatically gobbled anymore.  Thus, have to explicitly
  % get rid of it:
           before={\nowhitespace},
            after={\hrule height 2pt},
             page={yes},
            style={\bf\ssc},
  % ----------------------------------------------------------------------------
  % referenceprefix={chapter},
]
% ------------------------------------------------------------------------------
\setuphead
[section][%
            style={\bf\ssb},
  % ----------------------------------------------------------------------------
  % referenceprefix={section},
]
% ------------------------------------------------------------------------------
\setuphead
[subsection][%
            style={\bf\ssa},
  % ----------------------------------------------------------------------------
  % referenceprefix={subsection},
]
% ------------------------------------------------------------------------------
\setuphead
[title][%
            align={middle, nothyphenated, verytolerant},
  % When entering a page in normal typesetting mode, ConTeXt is in vertical
  % mode, and will automatically gobble all vertical whitespace that follows
  % immediately in order to align the first line of the text to the top of the
  % page.  That's why normally one does not see an extra vertical whitespace
  % before (above) the title which begins the page.  However, this is not the
  % case when entering the `makeup' environment since ConTeXt is in horizontal
  % mode then, and, as a result, the extra vertical whitespace before (above)
  % the title is not automatically gobbled anymore.  Thus, have to explicitly
  % get rid of it:
           before={\nowhitespace},
            after={},
  % Include in table of contents:
  incrementnumber={list},
           number={no},
             page={yes},
            style={\bf\ssc},
        textstyle={\WORDS},
  % ----------------------------------------------------------------------------
  % referenceprefix={},
]
% ------------------------------------------------------------------------------
\setuphead
[subsubsubject][%
  style={\bf\ss},
]
% ==============================================================================
% }}} Headings

\setuppublications
[alternative={aks}]

\def
\citepages{%
  \dodoubleempty
  \docitepages
}

\def
\citepages[#1][#2]#3#4{%
  \cite[extras={\doifsomethingelse{#2}{, \quotation{#2}}{}, pp.~#3--#4}][#1]%
}

\def
\citepage{%
  \dodoubleempty
  \docitepages
}

\def
\citepage[#1][#2]#3{%
  \cite[extras={\doifsomethingelse{#2}{, \quotation{#2}}{}, p.~#3}][#1]%
}

% Itemizes {{{
% ==============================================================================
\setupitemize[
      symbol={emdash},
    % symbol={\clap{---}},
  indentnext={auto},
       after={\nowhitespace},
      before={\nowhitespace},
   inbetween={\nowhitespace},
     stopper={.},
]
% ==============================================================================
% }}} Itemizes

% Tabulates {{{
% ==============================================================================
\definetabulate
[legend]
[|i1lme|p|]
% ------------------------------------------------------------------------------
\definetabulate
[legend]
[unit]
[|i1lm|cmRe|p|]
% ------------------------------------------------------------------------------
\definetabulate
[legend]
[value, unit]
[|i1lm|cm|rm|cmRe|p|]
% ------------------------------------------------------------------------------
\setuptabulate
[legend][
  indenting={no},
      split={yes},
       unit={1.5em},
         EQ={---},
      after={\nowhitespace},
     before={\blank[back]},
]
% ==============================================================================
% }}} Tabulates

% External Figures {{{
% ==============================================================================
\setupexternalfigures[
  directory={figures},
   maxwidth={\makeupwidth},
  maxheight={\makeupheight},
      scale={1000},
]
% ==============================================================================
% }}} External Figures

% Tables {{{
% ==============================================================================
\setupTABLE[
       split={repeat},
    % option={stretch},
  background={color},
]
% ------------------------------------------------------------------------------
\setupTABLE
[header]
[style={bold}]
% ------------------------------------------------------------------------------
\setupTABLE
[column]
[each]
[align={middle, lohi}]
% ------------------------------------------------------------------------------
\def
\externalTABLE{%
  \dosingleargument
  \doexternalTABLE
}
% ------------------------------------------------------------------------------
\def
\doexternalTABLE[#1]{%
  \bTABLE\input{#1.TABLE}\eTABLE%
}
% ==============================================================================
% }}} Tables

% Formulas {{{
% ==============================================================================
\setupformulas[
       indentnext={no},
  % ----------------------------------------------------------------------------
      numberstyle={bold},
  % ----------------------------------------------------------------------------
              way={bychapter},
           prefix={yes},
   prefixsegments={chapter},
  % ----------------------------------------------------------------------------
  referenceprefix={formula},
]
% ==============================================================================
% }}} Formulas

% Combinations {{{
% ==============================================================================
\setupcombinations[
  style={small},
  width={fit},
]
% ==============================================================================
% }}} Combinations

% Captions {{{
% ==============================================================================
\setupcaptions[
  numberconversion={Number},
]
% ------------------------------------------------------------------------------
\setupcaption
[figure][
            style={small},
        headstyle={bold},
  % ----------------------------------------------------------------------------
            width={\textwidth},
            align={middle},
         location={bottom},
  % ----------------------------------------------------------------------------
              way={bychapter},
           prefix={yes},
   prefixsegments={chapter},
  % ----------------------------------------------------------------------------
  referenceprefix={figure},
]
% ------------------------------------------------------------------------------
\setupcaption
[table][
            style={small},
        headstyle={bold},
  % ----------------------------------------------------------------------------
            width={\textwidth},
            align={right},
         location={top},
  % ----------------------------------------------------------------------------
              way={bychapter},
           prefix={yes},
           suffix={},
   prefixsegments={chapter},
  % ----------------------------------------------------------------------------
  referenceprefix={table},
]
% ==============================================================================
% }}} Captions

% Reference Formats {{{
% ==============================================================================
\unprotect

\definereferenceformat
[_indesigngoal]
[text={Design Goal~}]
% ------------------------------------------------------------------------------
\definereferenceformat
[_inissue]
[text={Issue~}]
% ------------------------------------------------------------------------------
\definereferenceformat
[_inoption]
[text={Option~}]
% ------------------------------------------------------------------------------
\definereferenceformat
[_inchapter]
[text={Chapter~}]
% ------------------------------------------------------------------------------
\definereferenceformat
[_insection]
[text={Section~}]
% ------------------------------------------------------------------------------
\definereferenceformat
[_insubsection]
[text={Subsection~}]
% ------------------------------------------------------------------------------
\definereferenceformat
[_infigure]
[text={Figure~}]
% ------------------------------------------------------------------------------
\definereferenceformat
[_intable]
[text={Table~}]
% ------------------------------------------------------------------------------
\definereferenceformat
[_informula][
   left={(},
  right={)},
]

\protect

\def
\refer{%
  \dodoubleargument
  \dorefer
}

\def
\dorefer[#1][#2]{%
  \csname _in#1\endcsname
  [#1:#2]%
}
% ==============================================================================
% }}} Reference Formats

% Quotations {{{
% ==============================================================================
\setupquotation[
   style={italic},
  before={\noindentation},
]
% ==============================================================================
% }}} Quotations

% Cited Quotation {{{
% ==============================================================================
\def
\startcitedquotation{%
  \dodoubleargument
  \dostartcitedquotation
}
% ------------------------------------------------------------------------------
\def
\dostartcitedquotation[#1][#2]{%
  \begingroup
    \setupquotation
    [right={\symbol[rightquotation]~\normal{\cite[#2]}}]
    \startquotation
    [#1]
}
% ------------------------------------------------------------------------------
\def
\stopcitedquotation{%
    \stopquotation
  \endgroup
}
% ==============================================================================
% }}} Cited Quotation

% TikZ {{{
% ==============================================================================
\tikzset{
  >={latex},
}
% \tikzset{external/force remake}
% \tikzexternalize
% ------------------------------------------------------------------------------
\def
\starttikz{%
  \dosingleargument
  \dostarttikz
}
% ------------------------------------------------------------------------------
\def
\dostarttikz[#1]{%
  \hbox
  \bgroup
    \scale[
       maxwidth={\externalfigureparameter{maxwidth}},
      maxheight={\externalfigureparameter{maxheight}},
    ]
    \bgroup
      \starttikzpicture
      [#1]%
}
% ------------------------------------------------------------------------------
\def
\stoptikz{%
      \stoptikzpicture
    \egroup
  \egroup
}
% ------------------------------------------------------------------------------
\def
\externaltikz{%
  \dodoubleargument
  \doexternaltikz
}
% ------------------------------------------------------------------------------
\def
\doexternaltikz[#1][#2]{%
  \doifsomethingelse%
  {#2}%
  {\starttikz[#2]\input{#1}\stoptikz}%
  {\starttikz\input{#1}\stoptikz}%
}
% ==============================================================================
% }}} TikZ

% Combination {{{
% ==============================================================================
\let
\corestartcombination
\startcombination
% ------------------------------------------------------------------------------
\def
\startcombination{%
  \dotripleargument
  \dostartcombination
}
% ------------------------------------------------------------------------------
\def
\dostartcombination[#1][#2][#3]{%
  \begingroup
    \doifsomethingelse
    {#3}
    {\def\w{#3}}
    {\def\w{\textwidth}}
    \def\width{%
      \dimexpr                                                                 %
      (                                                                        %
        \w                                                                     %
        +                                                                      %
        \combinationparameter{distance}                                        %
      )                                                                        %
      /                                                                        %
      #1                                                                       %
      -                                                                        %
      \combinationparameter{distance}                                          %
      \relax                                                                   %
    }
    \makeupwidth=\width
    % \setupexternalfigures
    % [maxwidth={\width}]
    \setupframedtexts[
       % width={\width},
      location={none},
    ]
    \setuptyping[
      location={none},
    ]
    \setupalgorithmic[
      location={none},
    ]
    \corestartcombination
    [#1*#2]%
}
% ------------------------------------------------------------------------------
\let
\corestopcombination
\stopcombination
% ------------------------------------------------------------------------------
\def
\stopcombination{%
    \corestopcombination
  \endgroup
}
% ==============================================================================
% }}} Combination

% URL {{{
% ==============================================================================
\definebar[urlunderbar][underbar]

\setupurl[
  style={\small\mono\urlunderbar},
  color={blue},
   left={<},
  right={>},
]
% ------------------------------------------------------------------------------
% \def
% \url{%
%   \dosingleargument
%   \dourl
% }
% % ------------------------------------------------------------------------------
% \def
% \dourl[#1]{%
%   \begingroup
%     \setupbar[%
%       style={\urlparameter{style}},
%       color={\urlparameter{color}},
%     ]%
%     % \setupinteraction[%
%     %   style={\urlparameter{style}},
%     %   color={\urlparameter{color}},
%     % ]%
%     % \mono\urlparameter{left}%
%     \from[#1]%
%     % \mono\urlparameter{right}%
%   \endgroup
% }
% ==============================================================================
% }}} URL

% Prose {{{
% ==============================================================================
\def
\y{%
  \doifnextcharelse{s}{ie}{y}%
}
% ------------------------------------------------------------------------------
\def
\etc{%
  \doifnextcharelse{.}{etc}{etc.\autoinsertnextspace}%
}
% ------------------------------------------------------------------------------
\def
\ie{%
  \doifnextcharelse{.}{i.e}{i.e.\ }%
}
% ------------------------------------------------------------------------------
\def
\eg{%
  \doifnextcharelse{.}{e.g}{e.g.\ }%
}
% ------------------------------------------------------------------------------
\def
\see{%
  \doifnextcharelse{.}{см}{см.\ }%
}
% ==============================================================================
% }}} Prose

% Math {{{
% ==============================================================================
\delimiterfactor=1100
% ------------------------------------------------------------------------------
\nulldelimiterspace=0pt
% ------------------------------------------------------------------------------
\everymath=\expandafter{\the\everymath\displaystyle}
% ------------------------------------------------------------------------------
\setupmathematics
[autopunctuation={all}]
% ------------------------------------------------------------------------------
\definemathmatrix
[matrix][
  style={\displaystyle},
  align={right},
  % ----------------------------------------------------------------------------
   left={\left( \,},
  right={\, \right)},
]
% ------------------------------------------------------------------------------
\definemathmatrix
[system][
     style={\displaystyle},
     align={right},
  distance={0.2278em},
  % ----------------------------------------------------------------------------
      left={\left\{ \,},
     right={\, \right.},
]
% ------------------------------------------------------------------------------
\definemathmatrix
[cases][
  style={\displaystyle},
  align={left},
  % ----------------------------------------------------------------------------
   left={\left\{ \,},
  right={\, \right.},
]
% ------------------------------------------------------------------------------
\def
\M{%
  \math{{\mathss M}}%
}
% ------------------------------------------------------------------------------
\def
\d{%
  \math{\mfunction{d}}%
}
% ------------------------------------------------------------------------------
\def
\D{%
  \math{\mfunction{D}}%
}
% ------------------------------------------------------------------------------
\def
\grad{%
  \math{\vec{\nabla}}%
}
% ------------------------------------------------------------------------------
\def
\const{%
  \math{\mfunction{const}}%
}
% ------------------------------------------------------------------------------
\def
\BGK{%
  \math{\text{BGK}}%
}
% ------------------------------------------------------------------------------
\def
\R{%
  \math{\reals}%
}
% ------------------------------------------------------------------------------
\def
\N{%
  \math{\naturalnumbers}%
}
% ------------------------------------------------------------------------------
\def
\Z{%
  \math{\integers}%
}
% ------------------------------------------------------------------------------
\def
\V{%
  \math{\mathbb{V}}%
}
% ------------------------------------------------------------------------------
\def
\O{%
  \math{\mathcal{O}}%
}
% ------------------------------------------------------------------------------
\def
\<{%
  \math{\langle}%
}
% ------------------------------------------------------------------------------
\def
\>{%
  \math{\rangle}%
}
% ------------------------------------------------------------------------------
\def
\unit#1{%
  \math{\left[#1\right]}%
}
% ==============================================================================
% }}} Math

\unprotect

\let
\_vec
\vec

\def
\vec#1{%
  \math{{\mathbi\_vec{#1}}}%
}

\protect

\def
\mat#1{%
  \math{{\mathbi#1}}%
}

\def
\transpose#1{%
  \math{#1^\top}%
}

\startsetups[makeup]
  % The `makeup' environment resets the global `indenting' and `whitespace'
  % settings, so reintroduce them here.
  \setupindenting[no]
  \setupwhitespace[line]
\stopsetups

\setupmakeup[
  doublesided={no},
  footerstate={start},
  headerstate={none},
    pagestate={start},
         page={yes},
       setups={makeup},
     commands={%
  % \setupwhitespace[line]%
  },
]

\setuptyping[
  indentnext={auto},
]

\setuplinenumbering
[location={text}]

\defineframedtext
[framedcode][
     align={flushleft},
    offset={0pt},
   loffset={4pt},
   roffset={4pt},
    corner={0},
     strut={no},
     width={\makeupwidth},
   % width={local},
]

\setupalgorithmic[
   indentnext={auto},
    numbering={no},
       margin={0pt},
       before={\startframedcode[location={\algorithmicparameter{location}}]},
        after={\stopframedcode},
  spacebefore={},
   spaceafter={},
]

\definetyping
[code][
     escape={[[,]]},
% numbering={line},
  numbering={no},
   bodyfont={script},
     before={\startframedcode
             [location={\namedtypingparameter{code}{location}}]},
      after={\stopframedcode},
]

\definestartstop
[todo][%
  before={\startframedcode%
          \bold{\WORDS{TODO}}\crlf},
   after={\stopframedcode},
]

% \definestructureconversionset[frontpart:pagenumber][][romannumerals]

\defineconversionset[frontpart:pagenumber][][romannumerals]
\defineconversionset[bodypart:pagenumber] [][numbers]
\defineconversionset[backpart:pagenumber] [][characters]

\setupuserpagenumber[way={byblock}]

\startsectionblockenvironment[frontpart]
% \setupuserpagenumber[numberconversion={romannumerals}]
\stopsectionblockenvironment

\startsectionblockenvironment[bodypart]
% \setcounter[userpage][1]
% \setupuserpagenumber[number={1}]
\stopsectionblockenvironment

\startsectionblockenvironment[backpart]
% \setcounter[userpage][1]
% \setupuserpagenumber[number={1}]
\stopsectionblockenvironment
