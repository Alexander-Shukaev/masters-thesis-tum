:: Preamble {{{
:: =============================================================================
::       @file build.cmd
:: -----------------------------------------------------------------------------
::     @author Alexander Shukaev <http://Alexander.Shukaev.name>
:: -----------------------------------------------------------------------------
:: @maintainer Alexander Shukaev <http://Alexander.Shukaev.name>
:: -----------------------------------------------------------------------------
::  @copyright Copyright (C) 2014,
::             Alexander Shukaev <http://Alexander.Shukaev.name>.
::             All rights reserved.
:: -----------------------------------------------------------------------------
::    @license This program is free software: you can redistribute it and/or
::             modify it under the terms of the GNU General Public License as
::             published by the Free Software Foundation, either version 3 of
::             the License, or (at your option) any later version.
::
::             This program is distributed in the hope that it will be useful,
::             but WITHOUT ANY WARRANTY; without even the implied warranty of
::             MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
::             General Public License for more details.
::
::             You should have received a copy of the GNU General Public License
::             along with this program. If not, see
::             <http://www.gnu.org/licenses/>.
:: =============================================================================
:: }}} Preamble

@echo off

setlocal EnableExtensions

set "PROJECT_DIR=%~dp0"
set "PROJECT_DIR=%PROJECT_DIR:~0,-1%"
set   "BUILD_DIR=%PROJECT_DIR%\.build"
set "FIGURES_DIR=%PROJECT_DIR%\figures"
set "SOURCES_DIR=%PROJECT_DIR%\sources"

set "TARGETS=thesis presentation"

md "%BUILD_DIR%" > nul
cd "%BUILD_DIR%" > nul

"robocopy.exe"                                                                 ^
  "%SOURCES_DIR%"                                                              ^
  "%BUILD_DIR%"                                                                ^
  /s                                                                           ^
  > nul

"robocopy.exe"                                                                 ^
  "%FIGURES_DIR%"                                                              ^
  "%BUILD_DIR%\figures"                                                        ^
  /s                                                                           ^
  > nul

call "+context"

for %%T in (%TARGETS%) do (
  "context.exe" "--path=%PROJECT_DIR%" "%%T.context"

  move /y "%BUILD_DIR%\%%T.pdf" "%PROJECT_DIR%" > nul
)

endlocal

:: Modeline {{{
:: =============================================================================
:: vim:ft=dosbatch:fenc=utf-8:ff=unix:fdm=marker:ts=2:sw=2:tw=80:et:
:: =============================================================================
:: }}} Modeline
