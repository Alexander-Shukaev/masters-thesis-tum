SocketCommunication(unsigned short     portNumber       = 0,
                    bool               reuseAddress     = false,
                    std::string const& networkName      = "lo",
                    std::string const& addressDirectory = ".");

SocketCommunication(std::string const& addressDirectory);
