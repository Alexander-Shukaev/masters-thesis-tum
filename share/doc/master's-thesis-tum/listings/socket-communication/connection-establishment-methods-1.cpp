void
acceptConnection(std::string const& nameAcceptor,
                 std::string const& nameRequester,
                 int                acceptorProcessRank,
                 int                acceptorCommunicatorSize);

void
requestConnection(std::string const& nameAcceptor,
                  std::string const& nameRequester,
                  int                requesterProcessRank,
                  int                requesterCommunicatorSize);
