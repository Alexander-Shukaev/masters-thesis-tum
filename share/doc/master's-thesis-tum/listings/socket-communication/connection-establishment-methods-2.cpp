void
acceptConnectionAsServer(std::string const& nameAcceptor,
                         std::string const& nameRequester,
                         int                requesterCommunicatorSize);

int
requestConnectionAsClient(std::string const& nameAcceptor,
                          std::string const& nameRequester);
