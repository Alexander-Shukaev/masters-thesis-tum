void
acceptConnection(std::string const& nameAcceptor,
                 std::string const& nameRequester);

void
requestConnection(std::string const& nameAcceptor,
                  std::string const& nameRequester);
